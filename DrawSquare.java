package drawshapesintheconsole;

public class DrawSquare {

    public static void main(String[] args) {

        int amount = 5;

        for (int i = 0; i < amount; i++) {
            for (int j = 0; j < amount; j++) {
                if (i == 0 || i == amount - 1 || j == 0 || j == amount - 1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println(" ");
        }

    }

}


/*

    int amount = 5;
        for (int i = 1; i < amount; i++) {
        for (int j = 0; j < amount; j++) {
        System.out.print((char) ((
        (-j) >> 32 & // encodes the condition: col > 0
        (-i) >> 32 &
        (j - (amount - 1)) >> 32 &
        (i - (amount - 1)) >> 32 & ('*' ^ '.')) ^ '*'));
        }
        System.out.println();
*/
